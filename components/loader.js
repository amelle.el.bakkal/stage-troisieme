export default function Loader({}) {
  return (
    <div className={`m-2 text-center`}>
      <span className={'loader'}></span>
    </div>
  )
}