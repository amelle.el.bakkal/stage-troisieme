export default function Footer({ siteName }) {
  return (
    <footer>
      <p className={'text-center'}><small>{siteName}</small></p>
    </footer>
  )
}